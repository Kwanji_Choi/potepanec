module Potepan::ProductsHelper
  def link_back
    if request&.referer&.include?('potepan/categories')
      :back
    else
      potepan_category_path(1)
    end
  end

  def sub_image(product)
    product.images.first || product.variant_images.first || Spree::Image.new
  end
end
