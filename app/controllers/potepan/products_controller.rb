class Potepan::ProductsController < ApplicationController
  MAX_DISPLAY_RELATED_PRODUCTS = 4
  before_action :set_product, only: [:show]
  def show
    @product_images = @product.variant_images.where(viewable_id: params[:id])
    @related_products = Spree::Product.related_products(@product).
      sample(MAX_DISPLAY_RELATED_PRODUCTS)
  end

  private

  def set_product
    @product = Spree::Product.find(params[:id])
  end
end
