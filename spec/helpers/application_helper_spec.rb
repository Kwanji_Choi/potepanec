require 'rails_helper'

RSpec.describe ApplicationHelper do
  context 'page_title' do
    it 'returns sample_title - Potepan EC' do
      expect(page_title(title: 'sample_title')).to eq 'sample_title - Potepan EC'
    end

    it 'returns Potepan EC when argument is nil' do
      expect(page_title(title: nil)).to eq "Potepan EC"
    end

    it 'returns Potepan EC when argument is empty' do
      expect(page_title(title: '')).to eq "Potepan EC"
    end

    it 'returns Potepan EC when no argument' do
      expect(page_title).to eq "Potepan EC"
    end
  end
end
