require 'rails_helper'

RSpec.describe Potepan::ProductsHelper do
  context 'link_back' do
    it 'returns :back when request.referer contains potepan/categories' do
      helper.request.env["HTTP_REFERER"] = 'http://example.com/potepan/categories/1'
      expect(helper.link_back).to eq :back
    end

    it "returns potepan_category_path(1)
              when request.referer doesn't contains potepan/categories" do
      helper.request.env["HTTP_REFERER"] = 'https://www.google.com/'
      expect(helper.link_back).to eq potepan_category_path(1)
    end

    it 'returns potepan_category_path(1) when request.referer is nil' do
      helper.request.env["HTTP_REFERER"] = nil
      expect(helper.link_back).to eq potepan_category_path(1)
    end
  end
end
