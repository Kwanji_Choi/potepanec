require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :system do
  let(:taxonomy)      { create(:taxonomy, name: 'test_taxonomy') }
  let(:taxon)         { create(:taxon, name: 'sample_category', parent: taxonomy.root) }
  let(:other_taxon)   { create(:taxon, name: 'other_category',  parent: taxonomy.root) }
  let(:product)       { create(:product, name: 'Sample Product', taxons: [taxon], price: 19.99) }
  let(:other_product) do
    create(:product, name: 'Other Product', taxons: [other_taxon], price: 29.99)
  end
  let!(:origin_taxon) { create(:taxon, id: 1) }

  before do
    product.master.images = [create(:image)]
    other_product.master.images = [create(:image)]
  end

  it 'user gets category page' do
    visit potepan_category_path(taxon.id)
    expect(page).to     have_title "sample_category - Potepan EC"
    expect(page).to     have_link nil, href: potepan_product_path(product.id)
    within("div.box-#{product.id}") do
      expect(page).to   have_content  'Sample Product'
      expect(page).to   have_content  '$19.99'
      expect(page).to   have_css      "img[src*='thinking-cat.jpg']"
    end
    expect(page).not_to have_link nil, href: potepan_product_path(other_product.id)
    expect(page).not_to have_selector "div.box-#{other_product.id}"
    within("ul.side-nav") do
      expect(page).to   have_link 'sample_category (1)', href: potepan_category_path(taxon.id)
      expect(page).to   have_link 'other_category (1)',  href: potepan_category_path(other_taxon.id)
    end
  end

  it 'user gets products#show from category#show and back to the category#show' do
    visit potepan_category_path(taxon.id)
    click_link href: potepan_product_path(product.id)
    find('.back-index').click
    expect(current_path).to eq potepan_category_path(taxon.id)
    visit potepan_category_path(other_taxon.id)
    click_link href: potepan_product_path(other_product.id)
    find('.back-index').click
    expect(current_path).to eq potepan_category_path(other_taxon.id)
  end

  it "user gets product#show directly and click back button" do
    visit potepan_product_path(product.id)
    find('.back-index').click
    expect(current_path).to eq potepan_category_path(1)
  end
end
