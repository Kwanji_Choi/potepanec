require 'rails_helper'

RSpec.describe "Potepan::Products", type: :system do
  let(:taxon)       { create(:taxon) }
  let(:other_taxon) { create(:taxon) }
  let(:product) do
    create(:product, name: 'Sample Product', taxons: [taxon],
                     description: 'Sample Product description', price: 19.99)
  end
  let!(:related_products) do
    create_list(:product, 5, name: 'Related Product', taxons: [taxon], price: 10.00)
  end
  let!(:not_related_product) do
    create(:product, name: 'Not Related Product', taxons: [other_taxon], price: 20.00)
  end

  before do
    related_products.each do |related_product|
      related_product.master.images = [create(:image)]
    end
  end

  it 'user gets product page' do
    visit potepan_product_path(product.id)
    expect(page).to have_title product.name
    expect(page).to have_link href: potepan_path, count: 3
    within 'div.singleProduct' do
      expect(page).to have_content 'Sample Product'
      expect(page).to have_content 'Sample Product description'
      expect(page).to have_content '$19.99'
    end
    within 'div.productsContent' do
      expect(page).to have_selector 'div.productBox',                count: 4
      expect(page).to have_content  'Related Product',               count: 4
      expect(page).to have_content  '$10.00',                        count: 4
      expect(page).to have_css      "img[src*='thinking-cat.jpg']",  count: 4
      expect(page).to have_css      "a[href*='/potepan/products/']", count: 4
    end
    expect(page).not_to have_content 'Not Related Product'
    expect(page).not_to have_content '20.00'
    expect(page).not_to have_link href: potepan_product_path(not_related_product.id)
  end
end
