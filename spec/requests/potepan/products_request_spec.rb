require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:product) do
    create(:product, name: 'Sample Product',
                     description: 'Sample Product description', price: 19.99)
  end

  describe ':show' do
    before do
      get potepan_product_path(product.id)
    end

    it 'response successfully' do
      expect(response).to have_http_status '200'
    end

    it 'renders products show' do
      expect(response).to render_template :show
    end

    it 'has product informations' do
      expect(response.body).to include 'Sample Product'
      expect(response.body).to include '19.99'
      expect(response.body).to include 'Sample Product description'
    end
  end
end
