require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let(:taxon) { create(:taxon, name: 'sample_category') }
  let(:product) do
    create(:product, name: 'Sample Product', taxons: [taxon], price: 19.99)
  end
  let!(:no_image_product) do
    create(:product, name: 'No Image Product', taxons: [taxon], price: 29.99)
  end

  before do
    product.master.images = [create(:image)]
  end

  describe ':show' do
    before do
      get potepan_category_path(taxon.id)
    end

    it 'response successfully' do
      expect(response).to have_http_status '200'
    end

    it 'renders products category' do
      expect(response).to render_template :show
    end

    it "has category name" do
      expect(response.body).to include 'sample_category'
    end

    it 'has products informations' do
      expect(response.body).to include 'Sample Product'
      expect(response.body).to include '19.99'
      expect(response.body).to include 'thinking-cat.jpg'
      expect(response.body).to include 'noimage'
    end
  end
end
