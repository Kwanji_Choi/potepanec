require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe 'scope' do
    describe 'related_products' do
      subject { Spree::Product.related_products(product) }

      let(:taxon)                { create(:taxon) }
      let(:other_taxon)          { create(:taxon) }
      let(:product)              { create(:product, taxons: [taxon]) }
      let!(:related_product)     { create(:product, taxons: [taxon]) }
      let!(:not_related_product) { create(:product, taxons: [other_taxon]) }
      let!(:both_taxons_product) { create(:product, taxons: [taxon, other_taxon]) }

      it { is_expected.to     include related_product }
      it { is_expected.not_to include product }
      it { is_expected.not_to include not_related_product }
      it { is_expected.to     include both_taxons_product }
    end
  end
end
